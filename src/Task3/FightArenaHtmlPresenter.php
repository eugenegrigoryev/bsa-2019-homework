<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        $fighters = $arena->all();
        foreach($fighters as $fighter) {
            $fighters_presentation .= "
                <p>{$fighter->getName()}: {$fighter->getHealth()}, {$fighter->getAttack()}</p> 
                <img src=\"{$fighter->getImage()}\"> <br>
            ";
        }
        return $fighters_presentation;
    }
}
