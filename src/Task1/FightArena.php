<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    
    public $fighters = [];
    
    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        $max_attack = 0;
        foreach($this->fighters as $fighter) {
            $attack = $fighter->getAttack();
            if($attack > $max_attack) {
                $max_attack = $attack;
                $most_powerful = $fighter;
            }
        }
        return $most_powerful;
    }

    public function mostHealthy(): Fighter
    {
        $max_health = 0;
        foreach($this->fighters as $fighter) {
            $health = $fighter->getHealth();
            if($health > $max_health) {
                $max_health = $health;
                $most_healthy = $fighter;
            }
        }
        return $most_healthy;
    }

    public function all(): array
    {
        return $this->fighters;
    }
}
